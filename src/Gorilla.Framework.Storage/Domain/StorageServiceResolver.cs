﻿using Gorilla.Framework.Storage.Domain.Enums;
using Gorilla.Framework.Storage.Domain.Interfaces.Services;
using Gorilla.Framework.Storage.Domain.Resolvers;
using Gorilla.Framework.Storage.Infra.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gorilla.Framework.Storage.Domain.Services
{
    /// <summary>
    /// Storage Service 
    /// </summary>
    public class StorageServiceResolver : IStorageServiceResolver
    {
        public virtual string StorageServiceProviderConfigurationKey()
        {
            return "Gorilla.Storage.Provider";
        }

        public IStorageService CreateStorageServiceInstance(enStorageServiceProviders provider)
        {
            IStorageService result;

            switch (provider)
            {
                case enStorageServiceProviders.MicrosoftAzure:
                    result = new AzureStorageService();
                    break;
                default:
                    throw new NotImplementedException(string.Format("Provider {0} is not implemented!", provider));
            }

            return result;
        }

        public IStorageService CreateStorageServiceInstance()
        {
            var providerName = System.Configuration.ConfigurationManager.AppSettings[this.StorageServiceProviderConfigurationKey()];

            System.Diagnostics.Debug.Assert(!string.IsNullOrWhiteSpace(providerName));

            enStorageServiceProviders provider = (enStorageServiceProviders)Enum.Parse(typeof(enStorageServiceProviders), providerName, true);

            return this.CreateStorageServiceInstance(provider);
        }
    }
}
