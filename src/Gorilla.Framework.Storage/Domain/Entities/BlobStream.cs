﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Gorilla.Framework.Storage.Domain.Enums;
namespace Gorilla.Framework.Storage.Domain.Entities
{
    /// <summary>
    /// Represents a Blob Stream. Represents the stored file
    /// </summary>
    public class BlobStream
    {
        /// <summary>
        /// Blobs unique id
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Blobs Content
        /// </summary>
        public Stream Content { get; set; }

        /// <summary>
        /// Blobs Unique URL. It`s the full path for accessing that file from the web.
        /// </summary>
        public string PublicUrl { get; protected set; }

        public BlobStream()
        {

        }

        public BlobStream(string sourceFilename) : this()
        {
            this.Reference = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(sourceFilename);
        }

        public static BlobStream Create(ref string finalFileName, Stream content)
        {
            finalFileName = $"{Guid.NewGuid().ToString().Substring(0, 6)}-{finalFileName}";

            return new BlobStream()
            {
                Reference = finalFileName,
                Content = content
            };
        }

        public void SetPublicUrl(string url)
        {
            this.PublicUrl = url;
        }
    }
}
