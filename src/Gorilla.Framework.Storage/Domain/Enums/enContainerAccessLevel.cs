﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gorilla.Framework.Storage.Domain.Enums
{
    /// <summary>
    /// Represents allowed access levels for storage containers
    /// </summary>
    public enum enContainerAccessLevel
    {
        /// <summary>
        /// Public access - all the container`s content is public allowed to anyone can access it
        /// </summary>
        Public = 0,

        /// <summary>
        /// Private access - only authenticatied or user that have a temporary access token can access the container`s content
        /// </summary>
        Private = 1
    }
}
