﻿using System;
using Gorilla.Framework.Storage.Domain.Enums;
using Gorilla.Framework.Storage.Domain.Interfaces.Containers;

namespace Gorilla.Framework.Storage.Domain.Interfaces.Services
{
    /// <summary>
    /// Represents the storage provider. 
    /// </summary>
    public interface IStorageService : IDisposable
    {
        /// <summary>
        /// Gets a Storage Provider`s Container instance. 
        /// </summary>
        /// <param name="containerName">Container`s name. Usually an app can have several containers. 
        /// For Instance : "temp" for temporary files, "public" for all public files and "private" for access restricted files
        /// </param>
        /// <param name="createIfNotExists">If it`s set to true creates a new Constainer if it doesn`t exist</param>
        /// <param name="accessLevel">Sets the access level for new containers</param>
        /// <returns></returns>
        IStorageContainer GetContainer(string containerName, bool createIfNotExists = true, enContainerAccessLevel? accessLevel = null);
    }
}
