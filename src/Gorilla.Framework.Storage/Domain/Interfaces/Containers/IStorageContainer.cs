﻿using Gorilla.Framework.Storage.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Gorilla.Framework.Storage.Domain.Enums;

namespace Gorilla.Framework.Storage.Domain.Interfaces.Containers
{
    /// <summary>
    /// Represents a Storage Container on the storage service
    /// </summary>
    public interface IStorageContainer
    {
        /// <summary>
        /// Storage Container's Unique Id
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Deletes that storage container from the storage provider
        /// </summary>
        Task DeleteAsync();

        void UploadBlob(BlobStream blob);

        /// <summary>
        /// Uploads the stream to the storage container
        /// </summary>
        Task UploadBlobAsync(BlobStream blob);

        /// <summary>
        /// Gets the stream from the storage container in memory. That should not be use to large files. 
        /// </summary>
        /// <param name="reference">blob`s unique id</param>
        Task<Stream> DownloadBlobAsync(string reference);

        /// <summary>
        /// Delets the blog from the storage container. That operation cannot be undone.
        /// </summary>
        /// <param name="reference">blob`s unique id</param>
        /// <returns></returns>
        Task DeleteBlobAsync(string reference);

        /// <summary>
        /// Updates the storage container access levels
        /// </summary>
        void SetAccessLevel(enContainerAccessLevel permission);

        /// <summary>
        /// Updates the storage container access levels
        /// </summary>
        Task SetAccessLevelAsync(Domain.Enums.enContainerAccessLevel accessLevel);

        IEnumerable<string> Logs { get; }

        /// <summary>
        /// Creates the storage container if it doesn`t exist
        /// </summary>
        void CreateIfNotExists();

        /// <summary>
        /// Creates the storage container if it doesn`t exist
        /// </summary>
        Task CreateIfNotExistsAsync();

        /// <summary>
        /// Returns true if the file exists
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        Task<bool> BlobExistsAsync(string reference);

        List<string> BlobsList();

        Task DownloadBlobAsync(string reference, Stream outputStream);
    }
}
