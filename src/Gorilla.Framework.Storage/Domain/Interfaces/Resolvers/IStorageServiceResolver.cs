﻿using System;
using Gorilla.Framework.Storage.Domain.Interfaces.Services;
using Gorilla.Framework.Storage.Domain.Enums;

namespace Gorilla.Framework.Storage.Domain.Resolvers
{
    /// <summary>
    /// Storage Resolver. Chooses the storage provider according to app settings
    /// </summary>
    public interface IStorageServiceResolver
    {
        /// <summary>
        /// Creates a new storage service instance according to the configuration key. 
        /// It reads the app.config/web.config file key ( the key is returned by "StorageServiceProviderConfigurationKey" ) and creates the storage service instance
        /// </summary>
        IStorageService CreateStorageServiceInstance();

        /// <summary>
        /// Creates a new storage service instance according to the storage provider
        /// </summary>
        /// <param name="provider">Storage Provider</param>
        IStorageService CreateStorageServiceInstance(enStorageServiceProviders provider);

        /// <summary>
        /// Returns the provider configuration key. It`s default value is "Gorilla.Storage.Provider"
        /// </summary>
        string StorageServiceProviderConfigurationKey();
    }
}
