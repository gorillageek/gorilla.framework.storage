﻿using Gorilla.Framework.Storage.Domain.Enums;
using Gorilla.Framework.Storage.Domain.Interfaces.Containers;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Gorilla.Framework.Storage.Infra.Providers.Azure
{
    public class AzureStorageContainer : IStorageContainer
    {

        private List<string> logs { get; set; }

        private CloudBlobClient storageClient = null;
        private CloudBlobContainer container = null;

        public AzureStorageContainer(CloudBlobClient storageClient, string containerName, bool createIfNotExists = true, enContainerAccessLevel? accessLevel = null)
        {
            System.Diagnostics.Debug.Assert(storageClient != null);
            System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(containerName));

            Name = containerName;
            this.storageClient = storageClient;

            Setup(createIfNotExists, accessLevel);
        }

        public void CreateIfNotExists()
        {
            container.CreateIfNotExists();
        }

        public async Task CreateIfNotExistsAsync()
        {
            await container.CreateIfNotExistsAsync();
        }

        public async Task<bool> BlobExistsAsync(string reference)
        {
            try
            {
                var blockBlob = container.GetBlockBlobReference(reference);
                await blockBlob.FetchAttributesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<string> BlobsList()
        {
            return container.ListBlobs()
                .OfType<CloudBlockBlob>()
                .Select(x => x.Name)
                .ToList();
        }

        protected bool Setup(bool createIfNotExists = false, enContainerAccessLevel? accessLevel = null)
        {
            bool result = false;

            try
            {
                container = storageClient.GetContainerReference(Name);

                if (createIfNotExists)
                {
                    container.CreateIfNotExists();
                }

                if (accessLevel.HasValue)
                {
                    SetAccessLevel(accessLevel.Value);
                }

                result = true;
            }
            catch (Exception ex)
            {
                logs.Add("Setup failed : " + ex.ToString());
                System.Diagnostics.Debug.Fail(ex.ToString());
            }

            return result;
        }

        public string Name { get; set; }

        public async Task DeleteAsync()
        {
            await container.DeleteIfExistsAsync();
        }

        IEnumerable<string> IStorageContainer.Logs => logs;

        public async Task UploadBlobAsync(Domain.Entities.BlobStream blob)
        {
            try
            {
                // Retrieve reference to a blob named "myblob".
                var blockBlob = container.GetBlockBlobReference(blob.Reference);

                if (blob.Content.Position > 0)
                {
                    blob.Content.Position = 0;
                }

                await blockBlob.UploadFromStreamAsync(blob.Content);

                blob.SetPublicUrl(blockBlob.Uri.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UploadBlob(Domain.Entities.BlobStream blob)
        {
            try
            {
                // Retrieve reference to a blob named "myblob".
                var blockBlob = container.GetBlockBlobReference(blob.Reference);

                if (blob.Content.Position > 0)
                {
                    blob.Content.Position = 0;
                }

                 blockBlob.UploadFromStream(blob.Content);

                blob.SetPublicUrl(blockBlob.Uri.ToString());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<Stream> DownloadBlobAsync(string reference)
        {
            try
            {
                var blockBlob = container.GetBlockBlobReference(reference);

                var tempFileName = Path.GetTempFileName();

                var stream = new MemoryStream();

                await blockBlob.DownloadToStreamAsync(stream);
                await stream.FlushAsync();

                return stream;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task DownloadBlobAsync(string reference, Stream outputStream)
        {
            var blockBlob = container.GetBlockBlobReference(reference);
            await blockBlob.DownloadToStreamAsync(outputStream);
        }

        public async Task DeleteBlobAsync(string reference)
        {
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(reference);

            await blockBlob.DeleteIfExistsAsync();
        }

        protected virtual BlobContainerPermissions TransalePermissions(enContainerAccessLevel accessLevel)
        {
            var permissions = new BlobContainerPermissions();

            switch (accessLevel)
            {
                case enContainerAccessLevel.Public:
                    permissions.PublicAccess = BlobContainerPublicAccessType.Blob;
                    break;
                case enContainerAccessLevel.Private:
                    permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                    break;
                default:
                    throw new NotImplementedException("Permission access not implemented");
            }

            return permissions;
        }

        public void SetAccessLevel(enContainerAccessLevel accessLevel)
        {
            container.SetPermissions(TransalePermissions(accessLevel));
        }

        public async Task SetAccessLevelAsync(enContainerAccessLevel accessLevel)
        {
            await container.SetPermissionsAsync(TransalePermissions(accessLevel));
        }
    }
}
