﻿using Gorilla.Framework.Storage.Domain.Enums;
using Gorilla.Framework.Storage.Domain.Interfaces.Containers;
using Gorilla.Framework.Storage.Domain.Interfaces.Services;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Gorilla.Framework.Storage.Infra.Providers
{
    public class AzureStorageService : IStorageService
    {
        private CloudBlobClient _storageClient = null;
        private CloudStorageAccount _storageAccount = null;

        protected virtual string ConnectionStringConfigurationKey()
        {
            return "Gorilla.Storage.ConnectionString";
        }

        protected CloudStorageAccount StorageAccount
        {
            get
            {
                return _storageAccount
                    ?? (_storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(this.ConnectionStringConfigurationKey())));
            }
        }

        protected CloudBlobClient StorageClient
        {
            get
            {
                if (_storageClient == null)
                {
                    _storageClient = this.StorageAccount.CreateCloudBlobClient();
                }
                return _storageClient;
            }
        }

        public IStorageContainer GetContainer(string containerName, bool createIfNotExists = true, enContainerAccessLevel? accessLevel = null)
        {
            IStorageContainer container = new Azure.AzureStorageContainer(this.StorageClient, containerName, createIfNotExists, accessLevel);

            return container;
        }

        public void Dispose()
        {

        }
    }
}
