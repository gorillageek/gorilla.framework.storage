﻿using Gorilla.Framework.Storage.Domain.Entities;
using Gorilla.Framework.Storage.Infra.Providers;
using Gorilla.Framework.Storage.Tests.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Gorilla.Framework.Storage.Tests
{
    [TestClass]
    public class AzureStorageServiceTest
    {
        const string CONTAINER_NAME = "gorilla-unit-tests";
        const string FILE_REFERENCE = "logo-image.jpg";

        protected BlobStream GetTestBlob()
        {
            var stream = new MemoryStream();

            Resources.TestImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

            var blob = new BlobStream()
            {
                Reference = FILE_REFERENCE,
                Content = stream
            };

            return blob;
        }

        //[TestMethod]
        public void Create_File()
        {
            AzureStorageService service = new AzureStorageService();

            var container = service.GetContainer(CONTAINER_NAME);
            var blob = this.GetTestBlob();

            container.UploadBlobAsync(blob).Wait();
        }
        
        //[TestMethod]
        public void Delete_File()
        {
            AzureStorageService service = new AzureStorageService();

            var container = service.GetContainer(CONTAINER_NAME);
            var blob = this.GetTestBlob();

            container.DeleteBlobAsync(FILE_REFERENCE).Wait();
        }

        //[TestMethod]
        public void Delete()
        {
            AzureStorageService service = new AzureStorageService();

            var container = service.GetContainer(CONTAINER_NAME);

            container.DeleteAsync().Wait();
        }

    }
}
