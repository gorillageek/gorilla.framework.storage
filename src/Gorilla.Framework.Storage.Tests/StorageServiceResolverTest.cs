﻿using Gorilla.Framework.Storage.Domain.Enums;
using Gorilla.Framework.Storage.Domain.Services;
using Gorilla.Framework.Storage.Infra.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gorilla.Framework.Storage.Tests
{
    [TestClass]
    public class StorageServiceResolverTest
    {
        [TestMethod]
        public void CreateStorageServiceInstance_By_Provider()
        {
            StorageServiceResolver resolver = new StorageServiceResolver();

            var instance = resolver.CreateStorageServiceInstance(enStorageServiceProviders.MicrosoftAzure);

            Assert.IsNotNull(instance);
            Assert.IsTrue(instance.GetType() == typeof(AzureStorageService));
        }

        //[TestMethod]
        //public void CreateStorageServiceInstance_By_App_Setting()
        //{
        //    StorageServiceResolver resolver = new StorageServiceResolver();

        //    var instance = resolver.CreateStorageServiceInstance();

        //    Assert.IsNotNull(instance);
        //    Assert.IsTrue(instance.GetType() == typeof(AzureStorageService));
        //}
    }
}
